# -*- coding: utf-8 -*-
"""
Created on Fri Jun 22 20:49:06 2018

@author: pharnisch
"""

import math
import scipy as sp
import scipy.integrate as spig
import numpy as np
import matplotlib.pyplot as plt

def create_hilbert(n):
    A = np.empty((n, n))
    for i in range(n):
        for j in range(n):
            A[i][j] = 1 / (1 + i + j)
    return A

def create_deviation(n):
    delta = np.empty(n)
    c = 10**-6 / np.sqrt(n)
    for i in range(n):
        if i % 2 == 0:
            delta[i] = c * 1
        else:
            delta[i] = - c * 1
    return delta

n = 15
alpha = 10**-6 # Flaschenhals des Experimentes
I = np.identity(n)
b_delta = create_deviation(n)

A = create_hilbert(n) # Hilbertmatrix
x_exakt = [1]*n
b = A.dot(x_exakt) # Ax=b

# TSVD

U, sigma, V_transponiert = np.linalg.svd(A)
for i in range(n):
    kappa = sigma[0] / sigma[i]
    if sigma[i] > 0 and kappa > (1 / alpha):
        sigma[i] = 0 # Wert abgeschnitten!
Sigma = sigma * I
A_alpha = U.dot(Sigma).dot(V_transponiert)
x_alpha = np.linalg.pinv(A_alpha).dot(b + b_delta)
print(x_alpha)

# Tikhonov-Regularisierung

A_plus = np.linalg.pinv(A)
left = (A.T).dot(A) + (alpha**2)*I
right = (A.T).dot(b_delta)
x_alpha = np.linalg.inv(left).dot(right)
print(x_alpha)



