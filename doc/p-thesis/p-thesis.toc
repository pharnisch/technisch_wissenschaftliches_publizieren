\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Kurzbeschreibung}{3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Vorwort}{5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Einleitender Teil zur Numerik}{9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Numerisches Lösen von Problemen}{9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Schwierigkeiten beim numerischen Lösen von linearen Gleichungssystemen}{9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Frage nach Lösungsstrategien für Problematik schlecht konditionierter Matrizen}{10}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Grundlagen zur weiteren Arbeit}{11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Pseudo-Inverse}{11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Singulärwerte}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Definition von Singulärwerten}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Singulärwertzerlegung}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.3}Lösungsansatz mit Singulärwerten}{12}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Vergleich zweier Verfahren}{13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Truncated Singular Value Decomposition}{13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Tikhonov-Regularisierung}{14}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Programm}{15}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Ergebnisse}{17}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Weitere Schritte}{19}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Restlicher Programmcode}{21}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Literatur}{23}
